# Brand: IRLink
# Model: IRLink.VS

begin remote

  name  IRLink.VS
  bits           16
  flags SPACE_ENC|CONST_LENGTH
  eps            30
  aeps          100

  header       8888  4444
  one           593  1648
  zero          593   521
  ptrail        583
  pre_data_bits   16
  pre_data       0xFF
  gap          96689
  toggle_bit_mask 0x0

      begin codes
          POWER                    0x30CF
          MUTE                     0xB04F
          SCREEN                   0x7887
          CLOCK                    0xF807
          FOLDER                   0x38C7
          ZOOM                     0xB847
          1                        0x00FF
          2                        0x807F
          3                        0x40BF
          4                        0xC03F
          5                        0x20DF
          6                        0xA05F
          7                        0x609F
          8                        0xE01F
          9                        0x10EF
          0                        0x50AF
          AUDIO                    0x28D7
          VIDEO                    0xA857
          DVD                      0xE817
          FOTO                     0x18E7
          LEFT                     0x48B7
          RIGHT                    0xC837
          UP                       0x08F7
          DOWN                     0x8877
          OKAY                     0x708F
          eBOOK                    0x9867
          TV                       0x58A7
          FM                       0xD827
          MENU                     0x22DD
          LB-UP                    0x02FD
          LB-DOWN                  0x827D
          RB-UP                    0x42BD
          RB-DOWN                  0xC23D
          PLAY                     0x12ED
          PAUSE                    0x926D
          STOP                     0x52AD
          RECORD                   0xD22D
          REWIND                   0x32CD
          FORWARD                  0xB24D
          PREVIOUS                 0x728D
          NEXT                     0xF20D
          EJECT                    0x0AF5
          GREEN-BTN                0x8A75
          YELLOW-BTN               0x4AB5
          BLUE-BTN                 0xCA35
      end codes

end remote


