#
# this config file was derived from BN59-00516A
#
# contributed by Matt Hirsch (matthew dot hirsch at gmail dot com)
#
# brand: Samsung
# model no. of remote control: BN59-00599A
# devices being controlled by this remote: LN4065F LCD TV
#

# Codes generated when in "TV" mode
begin remote

  name  Samsung_BN59-00599A_TV
  bits           16
  flags SPACE_ENC|CONST_LENGTH
  eps            30
  aeps          100

  header       4633  4321
  one           712  1520
  zero          712   398
  ptrail        704
  pre_data_bits   16
  pre_data       0xE0E0
  gap          108193
  toggle_bit      0


      begin codes
          POWER                    0x40BF
          SOURCE                   0x807F
          1                        0x20DF
          2                        0xA05F
          3                        0x609F
          4                        0x10EF
          5                        0x906F
          6                        0x50AF
          7                        0x30CF
          8                        0xB04F
          9                        0x708F
          DASH                     0xC43B
          0                        0x8877
          PREV_CH                  0xC837
          CH_LIST                  0xD629
          SLEEP                    0xC03F
          REW                      0xA25D
          STOP                     0x629D
          PLAY_PAUSE               0xE21D
          FF                       0x12ED
          VOL_UP                   0xE01F
          CH_UP                    0x48B7
          MUTE                     0xF00F
          VOL_DOWN                 0xD02F
          CH_DOWN                  0x08F7
          MENU                     0x58A7
          EXIT                     0xB44B
          UP                       0x06F9
          LEFT                     0xA659
          ENTER                    0x16E9
          RIGHT                    0x46B9
          DOWN                     0x8679
          INFO                     0xF807
          PIP                      0x04FB
          P.MODE                   0x14EB
          S.MODE                   0xD42B
          P.SIZE                   0x7C83
          STILL                    0x42BD
          MTS                      0x00FF
          SRS                      0x7689
          CAPTION                  0xA45B
          ANTENNA                  0x6C93
          WISELINK                 0x31CE
          REC                      0x926D
          ANYNET                   0xE916
          FAV-CH                   0x22DD
          AUDCH_UP                 0x4CB3
          AUDCH_DOWN               0xCC33
          RETURN                   0x1AE5
# the following buttons are the mode change buttons at the
# top of the remote, but they have the same pre data as the
# TV codes
          STB_MODE                 0xFE01
          CABLE_MODE               0xDE21
          VCR_MODE                 0x5EA1
          DVD_MODE                 0x7E81
          TV_MODE                  0xBE41
      end codes

end remote

# Codes generated when in "DVD" mode
begin remote

  name  Samsung_BN59-00599A_DVD
  bits           16
  flags SPACE_ENC|CONST_LENGTH
  eps            30
  aeps          100

  header       9113  4348
  one           689  1550
  zero          689   434
  ptrail        690
  repeat       9112  4339
  pre_data_bits   26
  pre_data       0x198133F
  gap          108204
  toggle_bit      0


      begin codes
          POWER                    0x8976
          1                        0x817E
          2                        0x41BE
          3                        0xC13E
          4                        0x21DE
          5                        0xA15E
          6                        0x619E
          7                        0xE11E
          8                        0x11EE
          9                        0x916E
          0                        0x01FE
          REW                      0xF906
          STOP                     0x45BA
          PLAY_PAUSE               0x05FA
          FF                       0x857A
          MENU                     0xD12E
          EXIT                     0x25DA
          UP                       0xE916
          LEFT                     0x59A6
          ENTER                    0xD926
          RIGHT                    0x9966
          DOWN                     0x19E6
          INFO                     0xF10E
      end codes

end remote


# Code generated when in "STB" mode
begin remote

  name  Samsung_BN59-00599A_STB
  bits           16
  flags SPACE_ENC|CONST_LENGTH
  eps            30
  aeps          100

  header       4598  4358
  one           657  1574
  zero          657   454
  ptrail        652
  pre_data_bits   16
  pre_data       0x9090
  gap          108178
  toggle_bit      0


      begin codes
          POWER                    0x40BF
          SOURCE                   0x807F
          1                        0x20DF
          2                        0xA05F
          3                        0x609F
          4                        0x10EF
          5                        0x906F
          6                        0x50AF
          7                        0x30CF
          8                        0xB04F
          9                        0x708F
          0                        0x8877
          PREV_CH                  0xC837
          SLEEP                    0xC03F
          REW                      0xA25D
          STOP                     0x629D
          PLAY_PAUSE               0xE21D
          FF                       0x12ED
          CH_UP                    0x48B7
          CH_DOWN                  0x08F7
          MENU                     0x58A7
          EXIT                     0xB44B
          UP                       0x06F9
          LEFT                     0xA659
          ENTER                    0x16E9
          RIGHT                    0x46B9
          DOWN                     0x8679
          INFO                     0xF807
          BLUE                     0x7C83
          FAV_CH                   0x22DD
          REC                      0x926D
      end codes

end remote


# Code generated when in "CABLE" mode
begin remote

  name  Samsung_BN59-00599A_CABLE
  bits           16
  flags SPACE_ENC|CONST_LENGTH
  eps            30
  aeps          100

  header       9078  4380
  one           651  1587
  zero          651   460
  ptrail        663
  repeat       9077  2167
  pre_data_bits   16
  pre_data       0xFF
  gap          107165
  toggle_bit      0


      begin codes
          POWER                    0xF00F
          1                        0x8877
          2                        0x48B7
          3                        0xC837
          4                        0x28D7
          5                        0xA857
          6                        0x6897
          7                        0xE817
          8                        0x18E7
          9                        0x9867
          0                        0x08F7
          CH_UP                    0x20DF
          CH_DOWN                  0xA05F
      end codes

end remote


# Code generated when in "VCR" mode
begin remote

  name  Samsung_BN59-00599A_VCR
  bits           16
  flags SPACE_ENC|CONST_LENGTH
  eps            30
  aeps          100

  header       4601  4357
  one           659  1576
  zero          659   452
  ptrail        651
  pre_data_bits   16
  pre_data       0xA0A0
  gap          108199
  toggle_bit      0


      begin codes
          POWER                    0x40BF
          1                        0x20DF
          2                        0xA05F
          3                        0x609F
          4                        0x10EF
          5                        0x906F
          6                        0x50AF
          7                        0x30CF
          8                        0xB04F
          9                        0x708F
          0                        0x8877
          REW                      0x18E7
          STOP                     0xA857
          PLAY_PAUSE               0x9867
          FF                       0x58A7
          CH_UP                    0x48B7
          CH_DOWN                  0x08F7
          MENU                     0xF807
          EXIT                     0xB847
          UP                       0x1EE1
          LEFT                     0x42BD
          ENTER                    0x827D
          RIGHT                    0xC23D
          DOWN                     0xAE51
          INFO                     0x7887
          REC                      0x28D7
      end codes

end remote
