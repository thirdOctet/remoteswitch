# this config file was generated with the help of
# xmode2-0.7.0 on 2004-11-12
#
# contributed by Adrien Beau <adrien.put_my_name_here@free.fr>
#
# brand:                       Akai
# model no. of remote control: RC-W212F
# devices being controlled by this remote: Akai VS-G37
#
# This remote is sold with the Akai VS-G37 VCR. On the front are
# keys to control the record and playback abilities of the VCR, on
# the back are keys to program future recordings, and to set up
# VCR channels and clock if need be. Only one of the sides is
# active at a given time, this is selected using the blue switch
# on the side of the remote.
#
# The remote uses a modified NEC encoding, it is unlikely you will
# be able to use it to control other devices. On the program side
# of the remote is an LCD screen, which displays various
# information, usually the current time. Future recordings can be
# set up on the remote (using the ShowView or Program keys) before
# they are beamed to the VCR with the Transmit key. Similarly, the
# time and channels can be set using the Clock_Channel key, and
# then sent to the VCR for clock synchronisation or programmed
# channels alteration.
#
# These three operations are done using a custom extension to the
# NEC encoding; the format of the bits is the same, the bytes are
# still sent by complementary pairs, but many more pairs than
# usual are sent.  For example, 18 bytes are sent to the VCR when
# the time is being set, instead of 4. Of course, these format are
# of no use to LIRC, so I have not attempted to decypher them. (I
# have looked at the data sent for time setting; it is quite
# straightforward.)
#
# Note that the "F" at the end of the remote name probably stands
# for "French", the language in which the various texts on the
# remote are written. Other language versions probably exist, and
# send the very same codes. I did not want to use French for the
# key names in this configuration file, so I tried to use the most
# sensible English equivalents.
#
# The timings and protocol information for the NEC encoding were
# obtained from http://www.ustr.net/infrared/nec.shtml
# This helped me ensure that the information below is accurate and
# can be used as a basis for other NEC-encoded remotes. I had to
# increase the eps to 60% before the remote could be read by my
# SIR dongle. The gap was measured with xmode2, and tuned to get
# better key repeats.

begin remote

  name  Akai_RC-W212F
  bits           32
  flags SPACE_ENC|REVERSE
  eps            60
  aeps          100

  header       8960  4200
  one           560  1680
  zero          560   560
  ptrail        560
  gap          85000
  repeat       8960  2240
  frequency    36000

      begin codes
#         Keys on the front of the remote.
          Power                    0x00000000EC137789
          Eject                    0x00000000EF107789
          Reset_Counter            0x00000000F50A7789
          TV/VCR                   0x00000000E8177789
          Program_Down             0x00000000F10E7789
          Program_Up               0x00000000F20D7789
          Reduce_Speed             0x00000000E41B7789
          Still_Image              0x00000000F6097789
          Increase_Speed           0x00000000E51A7789
          Previous_Track           0x00000000F7087789
          I-HQ                     0x00000000EB147789
          Next_Track               0x00000000F8077789
          Fast_Backward            0x00000000FD027789
          Play                     0x00000000F9067789
          Fast_Forward             0x00000000FE017789
          Record                   0x00000000E9167789
          Stop                     0x00000000FC037789
          Pause                    0x00000000F00F7789

#         Keys on the back of the remote. The ShowView,
#         Clock_Channel, Program and Transmit keys send no code on
#         their own. See above for explanations.
          One                      0x00000000BF407789
          Two                      0x00000000BE417789
          Three                    0x00000000BD427789
          Four                     0x00000000BC437789
          Five                     0x00000000BB447789
          Six                      0x00000000BA457789
          Seven                    0x00000000B9467789
          Eight                    0x00000000B8477789
          Nine                     0x00000000B7487789
          Zero                     0x00000000B6497789
          AV                       0x00000000B54A7789
          Display                  0x00000000AE517789
          Auto_Align               0x00000000E7187789
          Prog_Funct               0x00000000E11E7789
          Menu                     0x00000000B04F7789
          Up                       0x00000000A35C7789
          Down                     0x00000000A25D7789
          Left                     0x00000000A6597789
          Right                    0x00000000A7587789
          Cancel                   0x00000000A9567789
          OK                       0x00000000AF507789
          Interactive_Program      0x00000000A05F7789
          Program_List             0x00000000AB547789
          Timer                    0x00000000AC537789
      end codes

end remote

