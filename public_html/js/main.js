jQuery(document).ready(function($){
    
    //set up global url to be used in ajax requests
    //    var url = window.location.protocol + "//" + window.location.host + '/' + 'CI-RemoteSwitch/public_html/';
    
    /*
     * Form Validation for the login process
     * 
     */    
    $("#loginForm").validate({
        rules: {                    
            email:{
                required: true, 
                email:true
            },
            password:{
                required:true
            }
        },
        messages: {
            email:"Please enter a valid email address",
            required: "Please enter your details"            
        },
        submitHandler: function(form){            
            var formstr = $("#loginForm").serialize();            
            $.ajax({                
                type:'POST',
                url: 'login/loginCheck',
                data: formstr+"&action=ajax",
                dataType: 'json',
                success: function(data){       
                    if(data.error){
                        alert(data.error);
                    }else if(data.success){
                        window.location = 'viewdevices';
                    }
                    else if(data.exception){
                        alert(data.exception);
                    }
                //http://stackoverflow.com/questions/4611504/redirect-using-jquery
                    
                   
                },
                error:function(data){
                    console.log(JSON.stringify(data));
                }
            })
        } 
    }); /* END LOGIN FORM VALIDATION*/
    
    
    /**
     * Used to register a user
     * 
     */    
    $("#registerUserForm").validate({
        rules: {                    
            email:{
                required: true, 
                email:true
            },
            password:{
                required:true
            },
            password2:{
                equalTo: "#password1"
            }
        },
        messages: {
            email:"Please enter a valid email address",
            required: "Please enter your details"            
        },
        submitHandler: function(form){            
            var formstr = $("#registerUserForm").serialize();            
            $.ajax({                
                type:'POST',
                url: 'register/registerUser',
                data: formstr+"&action=ajax",
                dataType: 'json',
                success: function(data){
                    $('#formResult').empty();
                    if(data.success){
                        $('#registerUserForm')[0].reset() //http://stackoverflow.com/questions/6653556/$-javascript-function-to-clear-all-the-fields-of-a-form
                        $('#formResult').append(data.success + "<br/><br/> You will be redirected to the login page");
                        $('#formResult').fadeIn(2000, function(){
                            window.location = 'login';
                        });                        
                        
                    } else if(data.error){
                        $('#formResult').append(data.error);
                        $('#formResult').show();
                    } else if(data.exception){
                        $('#formResult').append(data.exception);
                        $('#formResult').show();
                    }
                                       
                },
                error:function(data){
                    $('#formResult').append(data);
                    $('#formResult').show();
                }
            })
        } 
    }); /* END LOGIN FORM VALIDATION*/
    
    /**
     *  Gets a device based on brand selection
     */
    $("#addmyDevice #brand").change(function(){
       
        var choice = $(this)[0].selectedIndex;
        if(choice==0){
            console.log("please select a device");
        }
        else{            
            choice = $("#addmyDevice #brand").val();
        }
        
        //http://api.jquery.com/jQuery.post/
        $.ajax({
            type:'POST',
            url: 'viewdevices/getModels',
            data: "&brand="+choice+"&action=ajax",
            dataType: 'json',
            success: function(data){                
                $("#addmyDevice #model").empty();
                $("#addmyDevice #model").append("<option>Select a Model</option>");
                for(var i=0;i<data.length;i++)
                {
                    $("#addmyDevice #model").append("<option>" + data[i].model + "</option>");
                }                                   
            },
            error: function(data){
            }
        });
    });
    
    /**
     * Adds user device to screen
     */
    $("#addmyDevice #addUserDevice").click(function(){
        if($("#addmyDevice #brand")[0].selectedIndex !==0 && $("#addmyDevice #model")[0].selectedIndex !==0){
            var brand = $("#addmyDevice #brand").val();
            var model = $("#addmyDevice #model").val();
            $.ajax({
                type:'POST',
                url: 'viewdevices/addDevice',
                data: "&action=ajax&model="+model+"&brand="+brand,
                dataType: 'json',
                success: function(data){
                    $('#status').empty();
                    $('#status').removeClass('requestSuccess');
                    $('#status').removeClass('requestError');
                    $('#status').removeClass('defaultStatus');
                    if(data.success){
                        $('#status').html('Status: Success <br/><br/>')
                        $('#status').addClass('requestSuccess');                        
                        $('#devicesContent tbody').append(data.success);     
                    } else if(data.exception){
                        $('#status').html('Status: Error <br/><br/>')
                        $('#status').addClass('requestError');
                        $('#status').append(data.exception);
                    } else if(data.error){
                        $('#status').html('Status: Error <br/><br/>')
                        $('#status').addClass('requestError');
                        $('#status').append(data.error);
                    //                        console.log(data.url);
                    }
                },
                error: function(data){
                    $('#status').empty();
                    $('#status').removeClass('requestSuccess');
                    $('#status').removeClass('defaultStatus');
                    $('#status').addClass('requestError');                   
                    $('#status').append(data.responseText);
                }
            });
        }
        else{
            $('#status').empty();
            $('#status').removeClass('requestSuccess');
            $('#status').removeClass('defaultStatus');
            $('#status').addClass('requestError');
            $('#status').html("Status: Error <br/><br/>Please choose a model");
        }
    });
    
    /**
     *  Get brand and model and remove from user database
     */    
    $(document).on('click',".remove",function(){
            
        var parentid = $(this).parent().attr('id');        
        
        var stringArray = parentid.split('__');
        
        var brand = stringArray[0];
        
        var model = stringArray[1];
        
        $.ajax({
            type:'POST',
            url: 'viewdevices/removeDevice',
            data: "&action=ajax&model="+model+"&brand="+brand,
            dataType: 'json',
            success: function(data){
                
                $('#status').empty();
                $('#status').removeClass('requestSuccess');
                $('#status').removeClass('requestError');
                $('#status').removeClass('defaultStatus');
                
                if(data.success){
                    $('#'+parentid).remove();  
                    $('#status').html('Status: Success <br/><br/>')
                    $('#status').addClass('requestSuccess');                        
                    $('#status').append(data.success);     
                } else if(data.exception){
                    $('#status').html('Status: Error <br/><br/>')
                    $('#status').addClass('requestError');
                    $('#status').append(data.exception);
                } else if(data.error){
                    $('#status').html('Status: Error <br/><br/>')
                    $('#status').addClass('requestError');
                    $('#status').append(data.error);
                }
            },
            error: function(data){
                $('#status').empty();
                $('#status').removeClass('requestSuccess');
                $('#status').removeClass('defaultStatus');
                $('#status').addClass('requestError');
                $('#status').html("Status: Error <br/><br/>" + data.responseText);
            }
        });
    });

    /**
     *  Get brand and model and remove from user database
     */    
    $(document).on('click',".commandSend",function(){
            
        var parentid = $(this).parent().parent().attr('id');
        
        var stringArray = parentid.split('__');        

        if($("#"+parentid + " select")[0].selectedIndex === 0){
            $('#status').empty();
            $('#status').removeClass('requestSuccess');
            $('#status').removeClass('requestError');
            $('#status').removeClass('defaultStatus');
            $('#status').html('Status: Error <br/><br/>')
            $('#status').addClass('requestError');
            $('#status').append("you must choose a command");
        }
        else{
            var command = $("#" + parentid + " select").val(); 
            var model = stringArray[1];
            //console.log(command)
            $.ajax({
                type:'POST',
                url: 'viewdevices/sendCommands',
                data: "&action=ajax&model="+model+"&command="+command,
                dataType: 'json',
                success: function(data){
                    $('#status').empty();
                    $('#status').removeClass('requestSuccess');
                    $('#status').removeClass('requestError');
                    $('#status').removeClass('defaultStatus');
                    
                    if(data.success){
                        $('#status').html('Status: Success <br/><br/>')
                        $('#status').addClass('requestSuccess');                        
                        $('#status').append(data.success + " " + command);     
                    } else if(data.exception){
                        $('#status').html('Status: Error <br/><br/>')
                        $('#status').addClass('requestError');
                        $('#status').append(data.exception);
                    } else if(data.error){
                        $('#status').html('Status: Error <br/><br/>')
                        $('#status').addClass('requestError');
                        $('#status').append(data.error);
                    //                        console.log(data.url);
                    }
                },
                error: function(data){
                    $('#status').empty();
                    $('#status').removeClass('requestSuccess');
                    $('#status').removeClass('defaultStatus');
                    $('#status').addClass('requestError');
                    $('#status').html("Status: Error <br/><br/>" + JSON.stringify(data));
                }
            });
        }        
    });  
}); /* END $ */