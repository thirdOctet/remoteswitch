<?php

Class FormManagement_Model extends CI_Model{

    var $formSource = '';

    /**
     * Capture form fields
     *  
     */
    var $firstname;
    var $surname;
    var $email;
    var $password;
    

    /**
     * 
     * @param string $val Takes form entry after submission and validates field
     * @return string
     */
    function sanitizeForm($val) {
        $val = trim($val);
        $val = mysql_real_escape_string($val);
        return $val;
    }
    
    /**
     * initialise all form fields
     */
    function getAllFormFields()
    {
        $this->firstname = $this->issetCheck("firstname");
        $this->surname = $this->issetCheck("surname");
        $this->email = $this->issetCheck("email");
        $this->password = md5($this->issetCheck("password"));
    }
    
    /**
     * Check if form field not emty and sanitize
     * 
     * @param string $field the name of the field from the form
     * @return string the result of the sanitisation
     */
    public function issetCheck($field){
        if(isset($_POST["$field"])){
            $result = $this->sanitizeForm($_POST["$field"]);
            return $result;
        }
        else{
            return '';
        }
    }
    
}

?>
