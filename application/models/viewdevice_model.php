<?php

Class Viewdevice_model extends CI_Model {

    //lirc location relative to web directory
    var $lircLoc = "../../../../../etc/lirc/lircd.conf";
    var $remotesLoc;

    /**
     * Inherits all functionality from base class
     */
    function __construct() {
        parent::__construct();
        
        //set base url for remotes
        $this->remotesLoc = base_url() . "remotes";
        
        //include phpseclib
        include(dirname(dirname(__FILE__)) . '/libraries/phpseclib/Net/SSH2.php');
    }

    /**
     * Gets a list of all registered devices for user
     *     
     * @param int $id The registered users id
     * @return array
     */
    public function getUserDevices($id) {

        try {
            //get all devices from database associated with user
            $this->db->select('devices.brand, devices.model, devices.commands');
            $this->db->from('userdevices');
            $this->db->join('devices', 'devices.id=userdevices.deviceid');
            $this->db->join('users', "users.id = userdevices.userid");
            $this->db->where('users.id', $id);
            $this->db->order_by('devices.brand');
            $query = $this->db->get();

            //get result from db
            $userDevices = $query->result();

            //ensures atleast one result
            if ($query->num_rows > 0) {
                //tests for request through ajax means

                return array('success' => $userDevices);
            } else {
                //sent from jquery                
                return array('error' => "Please add a device");
            }
        } catch (Exception $exc) {
            return array('exception' => $exc->getTraceAsString());
        }
    }

    /**
     * Creates the html structure for all registered devices of a user and 
     * incldes commands
     * 
     * @param type $id
     * @return type
     */
    public function htmlUserOutput($id) {
        try {
            $devicelist = $this->getUserDevices($id);

            if (array_key_exists('success', $devicelist)) {

                $html = "";

                foreach ($devicelist['success'] as $userdevice) {
                    $html.="<tr id='$userdevice->brand" . "__" . "$userdevice->model'>";
                    $html.="<td class='remove'><img src='" . base_url() . "imgs/remove.png' /></td>";
                    $html.="<td id='brand'>$userdevice->brand</td>";
                    $html.="<td id='model'>$userdevice->model</td>";
                    $html.="<td id='command'>";
                    $html.="<select>";
                    $html.="<option>Select a command</option>";
                    if ($userdevice->commands) {
                        $commands = $this->parseCommands($userdevice->commands);
                        foreach ($commands as $command) {
                            $html.="<option>$command</option>";
                        }
                    }
                    $html.="</select>";
                    $html.="</td>";
                    $html.="<td><div class='commandSend'>Send</div></td>";
                    $html.="</tr>";
                }

                return array('success' => $html);
            } else if (array_key_exists('error', $devicelist) || array_key_exists('exception', $devicelist)) {
                return array('error' => 'unable to get device commands, please try again later');
            }
        } catch (Exception $exc) {
            return array('exception' => $exc->getTraceAsString());
        }
    }

    /**
     * 
     * Send a command to a registered device
     * 
     * User will choose this command in view devices page
     * Returns a success or failure message to screen
     * 
     * @param type $model
     * @param type $action
     */
    public function sendCommands() {
        try {
            $model = "";
            $command = "";
            if (isset($_POST['model'])) {
                $model = $_POST['model'];
            }
            if (isset($_POST['command'])) {
                $command = mysql_real_escape_string($_POST['command']);
            }

            
            $lircModelName = $this->getDeviceLircName($model);
            $lircModelName = $lircModelName['success'];
            if (!empty($lircModelName)) {

                //send command to device
                exec("irsend Send_once $lircModelName $command 2>&1", $commandOutput);

                //check if there is an error
                if (empty($commandOutput)) {
                    die(json_encode(array('success' => 'your device received your command')));
                } else {
                    die(json_encode(array('error' => $commandOutput)));
                }
            }
        } catch (Exception $exc) {
            die(json_encode(array('exception' => $exc->getTraceAsString())));
        }
    }

    /**
     * Gets the actual lirc name contained within the configuration file
     * 
     * @param string $model
     * @access private
     */
    private function getDeviceLircName($model) {

        try {
            $model = strtolower($model);

            $lircModelName = "";

            //get device list
            exec('irsend LIST "" "" 2>&1', $totalDevices);


            if (count($totalDevices) < 1) {
                return array('error' => "Please add a device");
            } else {
                //get true lirc name
                foreach ($totalDevices as $selectedDevice) {
                    $splitDevice = explode(': ', $selectedDevice);
                    $deviceName = strtolower($splitDevice[1]);
                    if (strpos($deviceName, $model) !== false || strpos($model, $deviceName) !== false) {
                        $lircModelName = $deviceName;
                    }
                }
            }
            return array('success' => $lircModelName);
        } catch (Exception $exc) {
            return array('exception' => $exc->getTraceAsString());
        }
    }

    /**
     * Resets the lirc daemon when a new device is added
     * 
     * @return array key value pair with either error or success
     */
    private function daemonReset() {
        try {

            $result = "";

            //use of server_name ensures flexibility        
            $ssh = new Net_SSH2($_SERVER['SERVER_NAME']);

            if ($ssh->isConnected()) {
                $ssh->disconnect();
            }

            define('NET_SSH2_LOGGING', NET_SSH2_LOG_COMPLEX);

            //ensure login
            if (!$ssh->login('pi', 'raspbErry')) {
                $result = array('error' => 'couldn\'t connect');
            } else {
                $ssh->exec("sudo ../../../../../etc/init.d/lirc restart\n");
                $result = array('success' => 'daemon reset');
            }
            return $result;
        } catch (Exception $exc) {
            return array('exception' => $exc->getTraceAsString());
        }
    }



    //what if user wants to add a device
    /**
     * Add a Device to user 
     */
    public function addDevice() {
        try {
            //get brand and model
            $model = "";
            $brand = "";
            if (isset($_POST['model'])) {
                $model = $_POST['model'];
            }
            if (isset($_POST['brand'])) {
                $brand = $_POST['brand'];
            }

            //get user logged in
            $activeUser = $this->session->all_userdata();

            //get device id from device table
            $this->db->select('id');
            $this->db->from('devices');
            $this->db->where('model', $model);

            $query = $this->db->get();
            $dbResult = $query->result();

            //ensures result
            if ($query->num_rows > 0) {

                //get id
                $deviceID = $dbResult[0]->id;


                $array = array(
                    'userid' => $activeUser['id'],
                    'deviceid' => $deviceID
                );

                //free result
                $query->free_result();

                //get all devices from user
                $query = $this->db->select("*")->from('userdevices')->where($array);
                $query = $this->db->get();
                if ($query->num_rows > 0) {
                    die(json_encode(array('error' => 'You have already added this device.')));
                } else {
                    //set up data for insert
                    $data = array(
                        'userid' => $activeUser['id'],
                        'deviceid' => $deviceID
                    );

                    //clear previous db query
                    $query->free_result();

                    //insert query 
                    $query = $this->db->insert('userdevices', $data);

                    //ensures data is inserted into db                
                    if ($query == true) {

                        //check lirc file exists
                        if (file_exists("$this->lircLoc")) {

                            //ensure lirc.conf id writable
                            if (is_writable("$this->lircLoc")) {                                
                                
                                //recreate the lirc file
                                $fileUpdate = $this->addAllDevicesToLirc();

                                if (array_key_exists('error', $fileUpdate) || array_key_exists('exception', $fileUpdate)) {
                                    die(json_encode($fileUpdate));
                                } else {

                                    //no errors incurred so far continue with process
                                    //reset daemon
                                    $deviceListRefresh = $this->daemonReset();

                                    //because the daemon could not be reset it implies that the
                                    //ability to send commands to a device is not possible
                                    //either a reset or re-request will be required
                                    //this should not hinder further processing
                                    if (array_key_exists('error', $deviceListRefresh) || array_key_exists('exception', $deviceListRefresh)) {
                                        die(json_encode($deviceListRefresh));
                                    } else {

                                        $lircModelName = '';

                                        $lircModelName = $this->getDeviceLircName($model);

                                        if (array_key_exists('error', $lircModelName) || array_key_exists('exception', $lircModelName)) {

                                            die(json_encode($lircModelName));
                                        } else {

                                            //get lirc model name
                                            $lircModelName = $lircModelName['success'];

                                            $modelCommands = '';

                                            //check if commands here
                                            //get lirc model commands

                                            $modelCommands = $this->getDeviceCommands($lircModelName, $model); //no commands due to failed daemon reset
                                            if (array_key_exists('error', $modelCommands) || array_key_exists('exception', $modelCommands)) {
                                                die(json_encode($modelCommands));
                                            } else {

                                                $modelCommands = $modelCommands['success'];

                                                //html format for brand, model and commands
                                                $html = "<tr id='" . $brand . "__" . $model . "'>";
                                                $html .= "<td class='remove'><img src='" . base_url() . "imgs/remove.png' /></td>";
                                                $html .= "<td id='" . $brand . "'>$brand</td>";
                                                $html .= "<td id='" . $model . "'>$model</td>";
                                                $html .= "<td class='command'><select><option>Select a command</option>";
                                                foreach ($modelCommands as $command) {
                                                    $html.="<option>" . $command . "</option>";
                                                }
                                                $html .= "</select></td>";
                                                $html .= "<td><div class='commandSend'>Send</div></td>";
                                                $html .= "</tr>";
                                                die(json_encode(array('success' => $html, 'commands' => $modelCommands)));
                                            }
                                        }
                                    }
                                }
                            } else {
                                die(json_encode(array('error' => 'Due to file permissions we are unable to write to the configuration file. Please contact support')));
                            }
                        } else {
                            die(json_encode(array('error' => 'lirc configuration does not exist')));
                        }
                    } else {
                        die(json_encode(array("error" => 'Unable to add device to user')));
                    }
                }
            }
        } catch (Exception $exc) {
            die(json_encode(array("exception" => $exc->getTraceAsString())));
        }
    }

    /**
     * Removes a device from a registered user
     */
    public function removeDevice() {

        try {
            //get brand and model
            $model = "";
            $brand = "";
            if (isset($_POST['model'])) {
                $model = $_POST['model'];
            }
            if (isset($_POST['brand'])) {
                $brand = $_POST['brand'];
            }

            //get user logged in
            $activeUser = $this->session->all_userdata();

            //get device id from device table
            $this->db->select('id');
            $this->db->from('devices');
            $this->db->where('model', $model);

            $query = $this->db->get();
            $dbResult = $query->result();

            //ensures result
            if ($query->num_rows > 0) {

                //get id
                $deviceID = $dbResult[0]->id;

                $array = array(
                    'userid' => $activeUser['id'],
                    'deviceid' => $deviceID
                );

                $query->free_result();
                $query = $this->db->select("*")->from('userdevices')->where($array);
                $query = $this->db->get();
                if ($query->num_rows < 1) {
                    die(json_encode(array('error' => 'This device is not registered in your list.')));
                } else {
                    //set up data for insert
                    $data = array(
                        'userid' => $activeUser['id'],
                        'deviceid' => $deviceID
                    );

                    //clear previous db query
                    $query->free_result();

                    //remove device query 
                    $query = $this->db->delete('userdevices', $data);

                    //ensures data is inserted into db
                    if ($query == true) {

                        //remove device from lirc and re add all devices
                        $fileUpdate = $this->addAllDevicesToLirc();

                        if (array_key_exists('error', $fileUpdate) || array_key_exists('exception', $fileUpdate)) {

                            die(json_encode($fileUpdate));
                        } else {

                            $deviceListRefresh = $this->daemonReset();

                            if (array_key_exists('error', $deviceListRefresh) || array_key_exists('exception', $deviceListRefresh)) {
                                die(json_encode($deviceListRefresh));
                            } else {
                                die(json_encode(array('success' => 'Your device was successfully removed')));
                            }
                        }
                    } else {
                        die(json_encode(array('error' => 'The device could not be removed from the system')));
                    }
                }
            }
        } catch (Exception $exc) {
            die(json_encode(array('exception' => $exc->getTraceAsString())));
        }
    }

    /**
     * Gets all distinct devices from the database
     *  
     * @param string $filter you can use '*' or column names
     * @return array the list of all devices
     */
    public function getAllDevices($filter) {
        try {
            //get all devices from database associated with user
            $this->db->select("$filter");
            $this->db->from('devices');
            $this->db->distinct();

            $query = $this->db->get();
            $allDevices = $query->result();

            if ($query->num_rows > 0) {
                if (isset($_POST['action'])) {
                    die(json_encode($allDevices));
                } else {
                    return $allDevices;
                }
            }
        } catch (Exception $exc) {
            if (isset($_POST['action'])) {
                die(json_encode(array('exception' => $exc->getTraceAsString())));
            } else {
                return array('exception' => $exc->getTraceAsString());
            }
        }
    }

    /**
     * Gets all models based on ajax request from user device page
     * Check is in place for ajax request to ensure flexibility
     * @return array all brands for remotes in json format
     */
    public function getSelectedModels() {

        try {
            $brand = '';

            //this method will only be called when a brand is set
            if (isset($_POST['brand'])) {
                $brand = $_POST['brand'];
            }

            $this->db->select('model')->from('devices')->where('brand', "$brand");

            $query = $this->db->get();

            $allSelectedModels = $query->result();

            if ($query->num_rows > 0) {
                if (isset($_POST['action'])) {
                    die(json_encode($allSelectedModels));
                } else {
                    return $allSelectedModels;
                }
            }
        } catch (Exception $exc) {
            if (isset($_POST['action'])) {
                die(json_encode(array('exception' => $exc->getTraceAsString())));
            } else {
                return array('exception' => $exc->getTraceAsString());
            }
        }
    }

    /**
     * Get commands for single device
     */
    public function getDeviceCommands($lircmodelname, $model) {
        try {
            $lircName = $lircmodelname;

            //command to linux to retrieve device list
            exec('irsend LIST ' . $lircName . ' " " 2>&1', $result);

            if (count($result) > 0) {

                //parse list of commands
                $commandParser = $this->parseLircCommandsOutput($result);

                //parse commands into array
                $parsedcommands = implode(',', $commandParser);

                //pass commands to db
                $data = array(
                    'commands' => $parsedcommands
                );

                //update database
                $this->db->where('model', $model);
                $dbupdate = $this->db->update('devices', $data);

                //check if update successful
                if ($dbupdate !== false) {
                    return array('success' => $commandParser);
                } else {
                    return array('error' => $result);
                }
            }
        } catch (Exception $exc) {
            return array('exception' => $exc->getTraceAsString());
        }
    }

    /**
     * Takes linux output of lirc commands and returns array of command names
     * 
     * @param type $array
     * @return array
     */
    public function parseLircCommandsOutput($array) {
        $commandArray = array();
        foreach ($array as $command) {
            $command = explode(" ", $command);
            array_push($commandArray, $command[2]);
        }
        return $commandArray;
    }

    /**
     * Uses a string of comma separated values to format to an array
     * 
     * @param type $csv
     * @return type
     */
    private function parseCommands($csv) {
        $result = explode(',', $csv);
        return $result;
    }

    /**
     * Ignore method - gets all devices and appends to lirc configuration file
     * Debugging purposes only
     */
    public function addAllDevicesToLirc() {

        try {
            
            //get all distinct devices from database in userdevices table
            $this->db->select('devices.brand, devices.model');
            $this->db->from('userdevices');
            $this->db->join('devices', 'devices.id=userdevices.deviceid');
            $this->db->group_by('devices.model');
            $this->db->order_by('devices.brand', 'ASC');
            $query = $this->db->get();

            $dbResult = $query->result();

            //file contents holder
            $text = "";
            
            //loop through each result and get file contents
            foreach ($dbResult as $device) {
                $fileContents = file_get_contents($this->remotesLoc . "/" . $device->brand . "/" . $device->model);
                $text .= "$fileContents\n";
            }
            
            //append all file contents to lircd.conf file
            $result = file_put_contents($this->lircLoc, $text);

            //ensures file is written to and file length is greater than 0
            if ($result !== 0) {
                return array('success' => 'file updated');
            } else {
                return array('error' => 'failed to add device');
            }
        } catch (Exception $exc) {
            echo array('exception' => $exc->getTraceAsString());
        }
    }
}
?>
