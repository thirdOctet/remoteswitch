<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of register
 *
 * @author thirdOctet
 */
class Register_model extends CI_Model {

    var $firstname = '';
    var $surname = '';
    var $username = '';
    var $password = '';

    public function __construct() {
        parent::__construct();
    }

    /**
     * Gets form fields and adds user to database 
     */
    public function registerUser() {

        try {
            //validate form fields
//        $validateForm = new FormManagement();
            //get form fields
            if (isset($_POST['firstname'])) {
                $this->firstname = $this->sanitizeField($_POST['firstname']);
            }
            if (isset($_POST['surname'])) {
                $this->surname = $this->sanitizeField($_POST['surname']);
            }
            if (isset($_POST['email'])) {
                $this->username = $this->sanitizeField($_POST['email']);
            }
            if (isset($_POST['password'])) {
                $this->password = md5($this->sanitizeField($_POST['password']));
            }

            $data = array('username' => $this->username,
                'password' => $this->password,
                'firstname' => $this->firstname,
                'surname' => $this->surname
            );

            //check if user exists

            $this->db->select('username');
            $this->db->from('users');
            $this->db->where('username', $this->username);

            $query = $this->db->get();

            if ($query->num_rows < 1) {
                //add to db
                $this->db->insert('users', $data);
                echo json_encode(array('success' => "$this->username was successfully added to the database"));
            } else {
                //return user exists
                echo json_encode(array('error' => "user already exists, please choose a different name"));
            }
        } catch (Exception $exc) {
            echo json_encode(array('exception' => $exc->getTraceAsString()));
        }

        //output result to json        
    }

    /**
     * 
     * @param string $val Takes form entry after submission and validates field
     * @return string
     */
    function sanitizeField($val) {
        $val = trim($val);
        $val = mysql_real_escape_string($val);
        return $val;
    }

}

?>
