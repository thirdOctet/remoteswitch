<?php

class Login_model extends CI_Model {

    //initialises class with parent defaults
    function __construct() {
        parent::__construct();
    }

    /**
     * Verify user then load devices based on user
     */
    function verifyUser() {

        try {

            //load model for form management
            $this->load->model('FormManagement_Model', 'fields');

            //get all form fields submitted in $_POST
            $this->fields->getAllFormFields();

            //get email and password
            $email = $this->fields->email;
            $password = $this->fields->password;

            //get access details from database
            $query = $this->db->get_where('users', array('username' => $email));

            $userDetails = $query->result();

            //ensure query retrieves results
            if ($query->num_rows > 0) {

                //check if password matches
                if (strpos($userDetails[0]->password, $password) !== FALSE) {
                    //create session with logged in user details
                    $array = array('id' => $userDetails[0]->id,
                        'username' => $userDetails[0]->username,
                        'firstname' => $userDetails[0]->firstname);

                    $this->session->set_userdata($array);

                    //If login successful then send information back to javascript
                    if (isset($_POST['action'])) {
                        die(json_encode(array('success' => $array)));
                    } else {
                        return $array;
                    }
                } else {
                    die(json_encode(array('error' => "Your details are incorrect, please try again")));
                }
            } else {
                die(json_encode(array('error' => "No user registered, please register your details")));
            }
        } catch (Exception $exc) {
            if (isset($_POST['action'])) {
                die(json_encode(array('exception' => $exc->getTraceAsString())));
            } else {
                return array('exception' => $exc->getTraceAsString());
            }
        }
    }

}

?>
