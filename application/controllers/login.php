<?php

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {

        $this->load->view('header');
        $this->load->view('login');
        $this->load->view('footer');
    }

    /**
     * Verifies if a user is registered
     */
    public function loginCheck() {
        $this->load->model('Login_model');
        $this->Login_model->verifyUser();
    }

}

?>
