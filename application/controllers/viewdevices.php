<?php

class ViewDevices extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    /**
     * The view devices controller for the logged in user
     * It will check for a session and if this is not set the user will
     * be redirected to the login controller
     */
    function index() {

        //Retrieves the session information for a logged in user
        $activeUser = $this->session->all_userdata();

        //check for non active session
        if (!$activeUser['id']) {
            redirect('login');
            die();
        }

        //load the header
        $this->load->view('header');

        //load the model to access methods
        $this->load->model('Viewdevice_model');

        //get the users registered devices 
        $theUserDevices = $this->Viewdevice_model->htmlUserOutput($activeUser['id']);

        if (array_key_exists('error', $theUserDevices)) {
            $data['userdevices'] = "no devices added";
        } else {
            //store the devices for the view to access
            $data['userdevices'] = $theUserDevices['success'];
        }


        $data['user'] = $activeUser['firstname'];
        $data['commands'] = '';

        //gets all brands and will eventually be loaded into add device area
        $data['brand'] = $this->Viewdevice_model->getAllDevices('brand');

        //checks for a result
        if ($data) {
            $this->load->view('myDevices', $data);
        } else {
            $this->load->view('myDevices');
        }

        //load the footer view
        $this->load->view('footer');
    }

    /**
     * When loading the users page to allow device models to be loaded 
     * based on selected brand
     * 
     */
    public function getModels() {
        $this->load->model('Viewdevice_model');
        $this->Viewdevice_model->getSelectedModels();
    }

    /**
     * When called will redirect user to login screen and destroy session
     */
    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

    /**
     * Add user device
     */
    public function addDevice() {
        $this->load->model('Viewdevice_model');
        $this->Viewdevice_model->addDevice();
    }

    /**
     * Remove user device
     */
    public function removeDevice() {
        $this->load->model('Viewdevice_model');
        $this->Viewdevice_model->removeDevice();
    }

    /**
     * Send Device Command
     */
    public function sendCommands() {
        $this->load->model('Viewdevice_model');
        $this->Viewdevice_model->sendCommands();
    }

}

?>
