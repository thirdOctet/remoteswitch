<?php
class Register extends CI_Controller{
    
    function __construct() {
        parent::__construct();
    }
    
    function index(){
        $this->load->view('header');
        $this->load->view('register');
        $this->load->view('footer');
    }
    
    function registerUser(){
        $this->load->model('Register_model');
        $this->Register_model->registerUser();
    }
}
?>
