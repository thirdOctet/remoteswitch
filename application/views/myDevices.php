<!-- MAIN CONTENT -->

<div id="secNav">
    <ul>
        <li>Hi <?php echo ucwords($user); ?></li>
        <li><a href="<?php echo base_url(); ?>instructions">Instructions</a></li>
        <li><a href="<?php echo base_url(); ?>viewdevices">My Devices</a></li>
        <li><a href="<?php echo base_url(); ?>viewdevices/logout">Logout</a></li>
    </ul>
</div><!-- /Sec Nav -->
<div id="content">
    <h1>My Devices</h1>
    <div id="devicesContent">
        <p id="status" class="defaultStatus">Status</p>
        <h2>Below is a list of your devices <?php echo ucwords($user); ?></h2>
        <label>To control your device(s) select it and then choose the action below:</label>
        <table>
            <tbody>
                <tr>
                    <th>Edit</th>
                    <th>Device</th>
                    <th>Model</th>
                    <th>Commands</th>
                </tr>                
                <?php if ($userdevices) { ?>
                    <?php echo $userdevices; ?>
                <?php } else {
                    ?>
                    <tr>
                        <td id="addRequest">Please add a device</td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>        
    </div><!-- /Device Content -->
    <div id="addmyDevice">
        <h2>Add a Device</h2>
        <select id="brand">
            <option>Select a brand</option>
            <?php if (is_array($brand)) { ?>
                <?php foreach ($brand as $theBrand) { ?>
                    <option><?php echo $theBrand->brand; ?></option>
                <?php }
            } ?>
        </select>
        <select id="model">
            <option>Select a model</option>
        </select>
        <span id="addUserDevice">Add</span>        
    </div>
    <span class="clearfix"></span><!-- /Clearfix -->
</div><!-- /Content -->
<!-- /MAIN CONTENT -->
