<!DOCTYPE html>
<html>
    <head>
        <title>RemoteSwitch - Control your space</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>imgs/favicon.ico"/>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.js"></script>        
    </head>
    <body>
        <div id="container"> 
            <header>
                <div id="headerP">
                    <figure id="logo">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>imgs/remote_switch.png" alt="Remote Switch" title="Remoteswitch Home" /></a>
                    </figure><!-- /Logo -->
                    <ul id="login">
                        <?php
                        $activeUser = $this->session->all_userdata();

                        if (array_key_exists('id',$activeUser) == FALSE) {
                            ?>

                            <li><a href="<?php echo base_url(); ?>login">Login</a></li> or
                            <li><a href="<?php echo base_url(); ?>register">Register</a></li>

                        <?php } ?>
                    </ul><!-- /Login -->
                    <nav>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li><a href="<?php echo base_url(); ?>about">About</a></li>
                        </ul>
                    </nav>
                    <div class="clearfix"></div><!-- /Clearfix -->
                </div><!-- /HeaderP -->
            </header><!-- /Header -->