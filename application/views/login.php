<!-- MAIN CONTENT -->
<div id="content">
                <h1>Login</h1>
                <div id="loginSec">
                     <h2>Log In to Remote Switch</h2>
                     <form id="loginForm" action="" method="post">
                         <fieldset>
                             <p>
                                <label id="emailLabel" for="email">Email Address:</label>
                            </p>
                            <p>
                                <input type="text" class="required email" id="" name="email"/>
                            </p>
                            <p>
                                <label id="passwordLabel" for="password">Password:</label>
                            </p>
                            <p>
                                 <input type="password" class="required" id="password" name="password" />
                             </p>
                             <p>
                                 <a id="forgotPass" href="">Forgot your password?</a>
                             </p>
                             <input type="submit" id="submit" name="submit" value="LOG IN" /> 
                         </fieldset>
                     </form>
                </div><!-- /Login -->
                <div id="registerSec">
                    <h2>Are you new?</h2>
                    <p>
                        <label>This service provides you with a central management system for your devices.</label>
                    </p>
                    <p>
                        <input type="button" id="createAccount" name="createAccount" value="CREATE ACCOUNT" /> 
                    </p>
                </div><!-- /Register -->
                <span class="clearfix"></span><!-- /Clearfix -->
            </div><!-- /Content -->
<!-- /MAIN CONTENT -->