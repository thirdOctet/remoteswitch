<!-- MAIN CONTENT -->
<div id="content">
    <h1>Create an Account</h1>
    <div id="registerContent">
        <h2>Please fill out the following information to register for an account:</h2>
        <form id="registerUserForm" action="" method="post">
            <fieldset>
                <p>
                    <label for="Firstname">First Name</label>
                </p>
                <p>
                    <input type="text" id="name" class="required" name="firstname"/>
                </p>
                <p>
                    <label for="Surname">Last Name</label>
                </p>
                <p>
                    <input type="text" id="name" class="required" name="surname"/>
                </p>
                <p>
                    <label for="email">Email</label>
                </p>
                <p>
                    <input type="email" id="email" class="required email" name="email"/>
                </p>
                <p>
                    <label for="password1">Choose Password</label>
                </p>
                <p>
                    <input type="password" id="password1" class="required" name="password"/>
                </p>
                <p>
                    <label for="password2">Confirm Password</label>
                </p>
                <p>
                    <input type="password" id="password2" class="required" name="password2"/>
                </p>
            </fieldset>
            <p>
                <input type="submit"  id="submit" value="REGISTER" /> <!--id="registerForm"-->
            </p>
        </form>
            <div id="formResult"></div>
    </div><!-- /Register Sec -->
    <span class="clearfix"></span><!-- /Clearfix -->
</div><!-- /Content -->
<!-- /MAIN CONTENT -->
