
<!-- MAIN CONTENT -->
            <div id="content">
                <h1>About</h1>
                <div id="aboutContent">
                    Remote Switch are a small innovative company that are using Raspberry Pi 
                    Technology to create a central HUB to control Infrared Devices.
                    We are using the technology to benefit household users (specifically users 
                    that are disable and do not have great mobility) as well as anywhere that uses a 
                    number of IR devices such as TV’s, games consoles, Stereos etc…
                 </div>
                <span class="clearfix"></span><!-- /Clearfix -->
            </div><!-- /Content -->
<!-- /MAIN CONTENT -->