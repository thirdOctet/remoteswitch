<!-- /MAIN CONTENT -->
            <div id="iheader">
                <div id="headerS">
                    <div id="headerSLeft">
                        <h1>Welcome to Remote Switch</h1>
                        <h2>Your gateway to centrally managing your space.</h2>
                        <h3><a href="<?php echo base_url(); ?>register">Get started now</a></h3>
                    </div><!-- /HeaderSLeft -->
                    <div id="headerSRight">
                        <img src="<?php echo base_url(); ?>imgs/banner.png" alt="Banner" />
                    </div><!-- /HeaderSRight -->
                    <div class="clearfix"></div><!-- /Clearfix -->
                </div><!-- /HeaderS -->
            </div>
            <div id="content">
                <h1>Home</h1>
                <a href="<?php echo base_url(); ?>register">
                    <div id="register">
                        <h2>Register</h2>
                        <h3>
                            Get a free account.
                        </h3>
                    </div>
                </a><!-- /Register -->
                <a href="<?php echo base_url(); ?>viewdevices">
                    <div id="manage">
                        <h2>Manage</h2>
                        <h3>
                            Easily manage your devices.
                        </h3>
                    </div>
                </a><!-- /Manage -->
                <a href="<?php echo base_url(); ?>control">
                    <div id="control">
                        <h2>Control</h2>
                        <h3>
                            Take control of your space.
                        </h3>
                    </div>
                </a><!-- /Control -->
                <span class="clearfix"></span><!-- /Clearfix -->
            </div><!-- /Content -->
<!-- /MAIN CONTENT -->
