         <footer>
                <div id="legal">
                    &copy;<?php echo date('Y'); ?> Remoteswitch
                </div><!-- /Legal -->
                <figure>
                    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>imgs/fLogo.png" /></a>
                </figure>
                <div class="clearfix"></div><!-- /Clearfix -->
            </footer><!-- /Footer -->
        </div><!-- Container -->
        <script type="text/javascript" src="<?php echo base_url(); ?>js/main.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/script.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>js/validate.js"></script>
    </body>
</html>